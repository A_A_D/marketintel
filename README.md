# Market Intel

Market Intel est un site web, une plateforme de visualisation de données. 

Les données concernent les secteurs de l'éducation, de l'emploi, de l'immobilier, de la restauration, de l'automobile et de l'entrepreunariat.
Pour chaque secteur évoqué, nous avons créé une application pour sa visualisation avec des histogrammes, des camemberts ou encore des des diagrammes en ligne. 

Django(framework) est la technologie utilisée pour développer Market Intel.

## Applications

Ainsi, immodashboard est l'application réalisée pour les différentes offres immobilières.

Carsdashboard est l'application qui concerne les offres automobiles.

Resto concerne les différentes entreprises de restauration.

CompanyDashboard et emploisdashboard sont respectivement les applications pour la visualisations des entreprises et des offres d'emplois.

# Contributeurs

Mes collègues et moi l'avons réalisé dans le cadre d'un stage à Volkeno. 

Nous étions au nombre de 5: 
- Aliou Abou DIALLO
- Amadou GUEYE
- Alioune MBAYE
- Serigne C. T. S. NDIAYE
- Baye Dame THIAM


Si vous voulez vous aussi travailler sur Market Intel, suivez les indications suivantes. 

## Pré-requis
- Installer Python
Vous pouvez installer Python facilement grâce à internet.
- Installer Django
```bash
pip install django
```

Après avoir cloné le projet et créé votre environnement virtuel, placez vous dans le répertoire contenant le fichier 'manage.py' puis exécutez les commandes suivantes.

## Dépendances et BDD

```bash
pip install -r requirements.txt
python ./manage.py makemigrations immodashboard
python ./manage.py makemigrations carsdashboard
python ./manage.py makemigrations CompanyDashboard
python ./manage.py makemigrations resto
python ./manage.py makemigrations emploisdashboard

python ./manage.py migrate
```

## Testez

Exécutez : 
```bash
python manage.py runserver
```


Puis rendez-vous dans votre navigateur à l'adresse [localhost](http:localhost:8000)


