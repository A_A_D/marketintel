#Importons les modules nécessaires
from django.urls import path
from . import views

#Définissons les routes vers nos vues
urlpatterns = [
    path('', views.connexion, name = "connexion"),
    path('inscription', views.inscription, name = "inscription"),
    path('accueil', views.accueil, name = "accueil"),
]
