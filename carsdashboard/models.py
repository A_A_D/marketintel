from django.db import models


class Cars(models.Model):
    id = models.IntegerField(primary_key=True)
    constructeur = models.TextField(blank=True, null=True)
    modele = models.TextField(blank=True, null=True)
    kilometrage = models.TextField(blank=True, null=True)
    carburant_field = models.TextField(db_column='carburant', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    annee = models.TextField(blank=True, null=True)
    localisation = models.TextField(blank=True, null=True)
    contact = models.TextField(blank=True, null=True)
    transmission = models.TextField(blank=True, null=True)
    prix = models.FloatField(blank=True, null=True)
    vendeur_field = models.TextField(db_column='vendeur', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'cars'
