
from django.urls import path 
from . import views


""" différent chemain pour les vues creer"""
urlpatterns = [
    path('accueil/', views.acceuil, name='index'),#page d'accueil
    path('statistiques/', views.donnee, name='statistique'),#page des cammembert de chaque pays
    path('capitales/', views.capitale, name='capitales'),#page du diagramme à barre représentant le nombre de restaurants par capitale 
    path('recherche/', views.recherche, name='recherche'),#page permettant de faire des recherches
    path('recherche/<int:id>/', views.regarder, name='regarder'), #affichage des détails pour un médicament
]